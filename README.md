# Lyon

A C# converter for the `pbn` format, used in Tribute Games' games.

Supports converting from and to: `zpbn` (compressed Paris binary), `pbn` (Paris binary), `yaml` (YAML).

Made to aid in modding [Flinthook](http://flinthook.com). [More info](https://zatherz.eu/flinthook).
