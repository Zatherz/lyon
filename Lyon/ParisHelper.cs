﻿using System;
using Paris;
using Paris.Engine;
using System.IO.Compression;
using System.IO;
using YamlDotNet.Serialization;
using System.Reflection;
using System.Collections.Generic;

namespace Lyon {
    public class ParisHelper {
        static ParisHelper() {
            // initializes the singleton
            new Paris.Engine.Context.ContextManager(l_services: null);
        }

        private class YAMLWrapper {
            [YamlMember(Alias = "content")]
            public object Content { get; set; }

            [YamlMember(Alias = "NOTE")]
            public string _Note { get; set; } = "Don't edit the fields below!";

            [YamlMember(Alias = "type")]
            public string Type { get; set; }

            [YamlMember(Alias = "rw_type")]
            public string ReaderWriterType { get; set; }
        }

        private static Deserializer _Deserializer = new DeserializerBuilder().Build();
        private static Serializer _Serializer = new SerializerBuilder().Build();
        public string Path { set; private get; }
        public bool Compressed { set; private get; }

        private BinaryReadWriter _BinaryReadWriter;

        public ParisHelper(string path, bool compressed = true) {
            Path = path;
            Compressed = compressed;
        }

        public ParisHelper(string yaml) {
            var wrapped = _Deserializer.Deserialize<YAMLWrapper>(yaml);
            Type type = Paris.Engine.System.Helper.Helpers.GetType(wrapped.ReaderWriterType);
            if (type != null) {
                _BinaryReadWriter = BinaryContentManager.Singleton.FindCreateReadWriter(type);
            } else {
                throw new NotImplementedException(string.Format("Reader for type {0} not found.", wrapped.Type));
            }
        }

        private Stream _GetReadStream() {
            var stream = File.OpenRead(Path);
            if (Compressed) return new DeflateStream(stream, CompressionMode.Decompress);
            return stream;
        }

        private Stream _GetWriteStream() {
            var stream = File.OpenWrite(Path); ;
            if (Compressed) return new DeflateStream(stream, CompressionMode.Compress);
            return stream;
        }

        private BinaryReadWriter _GetReadWriter(Stream stream) {
            using (BinaryReader binaryReader = new BinaryReader(stream)) {
                return BinaryContentManager.Singleton.ReadTypeReader(binaryReader);
            }
        }

        public BinaryReadWriter GetReadWriter() {
            if (_BinaryReadWriter != null) return _BinaryReadWriter;
            using (var stream = _GetReadStream()) {
                return _GetReadWriter(stream);
            }
        }

        private object _Load(Stream stream) {
            return BinaryContentManager.Singleton.Load(stream);
        }

        public object Load() {
            using (var stream = _GetReadStream()) {
                return _Load(stream);
            }
        }

        public List<string> GetDependencies(object o) {
            return GetReadWriter().GetDendencies(o);
        }

        public void Decompress(Stream stream) {
            if (!Compressed) throw new Exception("Not compressed");
            using (var bstream = _GetReadStream()) {
                bstream.CopyTo(stream);
            }
        }

        public byte[] Decompress() {
            using (var memstream = new MemoryStream()) {
                Decompress(memstream);

                return memstream.ToArray();
            }
        }

        public string DecompressToString() {
            return System.Text.Encoding.UTF8.GetString(Decompress());
        }

        public static void Compress(Stream stream, byte[] saved) {
            using (var deflate = new DeflateStream(stream, CompressionLevel.Optimal)) {
                deflate.Write(saved, 0, saved.Length);
            }
        }

        public static byte[] Compress(byte[] saved) {
            using (var stream = new MemoryStream(saved)) {
                Compress(stream, saved);
                return stream.ToArray();
            }
        }

        public void Save(Stream stream, object o) {
            using (var writer = new BinaryWriter(stream)) {
                writer.Write(GetReadWriter().GetType().FullName);

                BinaryReadWriter binaryReadWriter = GetReadWriter();
                if (binaryReadWriter != null) {
                    using (BinaryWriter binaryWriter = new BinaryWriter(stream)) {
                        binaryReadWriter.Write(binaryWriter, o);
                        binaryReadWriter.EndWrite(binaryWriter, o);
                    }
                } else {
                    throw new NotSupportedException("Unsupported type");
                }
            }
        }

        public byte[] Save(object o) {
            using (var stream = new MemoryStream()) {
                Save(stream, o);
                return stream.ToArray();
            }
        }

        public string SaveToString(object o) {
            return System.Text.Encoding.UTF8.GetString(Save(o));
        }

        public string Serialize(object o) {
            var wrapper = new YAMLWrapper { Type = o.GetType().AssemblyQualifiedName, ReaderWriterType = GetReadWriter().GetType().FullName, Content = o };
            return _Serializer.Serialize(wrapper);
        }

        public void Serialize(Stream stream, object o) {
            var bytes = System.Text.Encoding.UTF8.GetBytes(Serialize(o));
            stream.Write(bytes, 0, bytes.Length);
        }

        public static object Deserialize(string s) {
            var wrapper = _Deserializer.Deserialize<YAMLWrapper>(s);
            var type = Type.GetType(wrapper.Type, throwOnError: true);

            var obj_reserialized = _Serializer.Serialize(wrapper.Content);

            object deser = _Deserializer.Deserialize(obj_reserialized, type);
            return deser;
        }
    }
}
