﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Lyon {
    public class Resource {
        public object Object;
        public ParisHelper ParisHelper;
    }
    public abstract class Format {
        public string Name;
        public string FullName;
        public string[] Extensions;

        public Format(string full_name, string name, string[] exts) {
            FullName = full_name;
            Name = name;
            Extensions = exts;
        }

        public abstract Resource Load(string path);
        public abstract void Save(Stream stream, Resource res);

        public override string ToString() {
            return $"{FullName}/{Name}";
        }
    }

    public class FormatContainer {
        public List<Format> Formats = new List<Format>();

        public void Add(Format format) {
            Formats.Add(format);
        }

        public Format GuessFromExtension(string path) {
            if (path.Length <= 1) return null;
            var ext = Path.GetExtension(path).Substring(1); // without dot
            foreach (var format in Formats) {
                if (Array.Exists(format.Extensions, (s) => s == ext)) return format;
            }
            return null;
        }

        public Format FromName(string name) {
            foreach (var format in Formats) {
                if (format.Name == name) return format;
            }
            return null;
        }        
    }
}
