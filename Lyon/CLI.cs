﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using NMaier.GetOptNet;
using System.Reflection;
using Lyon.Formats;

namespace Lyon {
    [GetOptOptions(
        OnUnknownArgument = UnknownArgumentsAction.Throw,
        UsageIntro = "Usage: {{{PROGNAME}}} [--help|-h] [options...] INPUT [-o OUTPUT]",
        UsageEpilog = "Available formats:\n   yaml (YAML)\n   pbn (Paris binary)\n   zpbn (Compressed Paris binary)\n"
    )]
    public class Options : GetOpt {
        public string InputPath { get { return Parameters[0]; } }

        public Stream OutputStream {
            get {
                if (OutputPath == "-") return Console.OpenStandardOutput();
                return File.OpenWrite(OutputPath);
            }
        }

        [Parameters(Min = 0, Max = 1)]
        public List<string> Parameters = new List<string>();

        [Argument("stacktrace", HelpText = "Print stacktrace on error.")]
        [ShortArgument('s')]
        [FlagArgument(true)]
        public bool Stacktrace;

        [Argument("input-format", HelpText = "Input file format. Lyon tries to guess it based on the file name. Will error if it's not possible to guess.")]
        [ShortArgument('I')]
        public string InputFormat;

        [Argument("output-format", HelpText = "Output file format. Lyon tries to guess it based on the file name and the input path.")]
        [ShortArgument('O')]
        public string OutputFormat;

        [Argument("help", HelpText = "Show help.")]
        [ShortArgument('h')]
        [FlagArgument(true)]
        public bool HelpRequested;

        [Argument("output", HelpText = "Set output path (single hyphen for stdout).")]
        [ShortArgument('o')]
        [FlagArgument(true)]
        public string OutputPath = "-";

        [Argument("assembly", HelpText = "Load a C# .exe/.dll assembly (to make use of the type readers from it).")]
        [ShortArgument('a')]
        public List<string> Assemblies = new List<string>();
    }

    class CLI {
        public static Options Options = new Options();
        public static FormatContainer Formats = new FormatContainer { Formats = {
                new CompressedParis(),
                new UncompressedParis(),
                new YAML()
        }};

        private static string _usage = null;
        public static string Usage {
            get {
                if (_usage != null) return _usage;
                return _usage = Options.AssembleUsage(Console.WindowWidth).Replace("{{{PROGNAME}}}", AppDomain.CurrentDomain.FriendlyName);
            }
        }

        public static void PrintUsage() {
            Console.Write(Usage);
        }

        public static int Main(string[] args) {
            try { Options.Parse(args); } catch (GetOptException e) {
                Console.WriteLine(e.Message);
                PrintUsage();
                return 1;
            }

            if (Options.HelpRequested) {
                PrintUsage();
                return 0;
            }

            if (Options.Parameters.Count < 1) {
                PrintUsage();
                return 1;
            }

            if (Options.InputFormat != null && Formats.FromName(Options.InputFormat) == null) {
                Console.WriteLine($"Invalid input format '{Options.InputFormat}'");
                PrintUsage();
                return 1;
            }
            if (Options.OutputFormat != null && Formats.FromName(Options.OutputFormat) == null) {
                Console.WriteLine($"Invalid output format '{Options.OutputFormat}'");
                PrintUsage();
                return 1;
            }

            Format input_format;
            if (Options.InputFormat == null) input_format = Formats.GuessFromExtension(Options.InputPath);
            else input_format = Formats.FromName(Options.InputFormat);

            if (input_format == null) {
                Console.WriteLine("Couldn't figure out input format, please use -Iformat");
                return 1;
            }

            Format output_format = null;
            if (Options.OutputFormat == null && Options.OutputPath != null) output_format = Formats.GuessFromExtension(Options.OutputPath);
            else if (Options.OutputFormat != null) output_format = Formats.FromName(Options.OutputFormat);

            if (output_format == null) {
                if (input_format is CompressedParis || input_format is UncompressedParis) output_format = new YAML();
                else output_format = new CompressedParis();
            }

            foreach (var asmpath in Options.Assemblies) {
                AppDomain.CurrentDomain.Load(Assembly.LoadFile(asmpath).GetName());
            }

            Resource res;

            try {
                res = input_format.Load(Options.InputPath);
            } catch (FileNotFoundException e) {
                Console.WriteLine($"File '{Options.InputPath}' doesn't exist");
                if (Options.Stacktrace) Console.WriteLine(e.StackTrace);
                return 1;
            } catch (Exception e) {
                Console.WriteLine($"Failed opening file '{Options.InputPath}': {e.Message}");
                Console.WriteLine($"Are you sure you chose the right format? ({input_format})");
                var guessed_format = Formats.GuessFromExtension(Options.InputPath);

                if (guessed_format != null && guessed_format.GetType() != input_format.GetType()) {
                    Console.WriteLine($"Did you mean to use the {guessed_format} format?");
                }
                if (Options.Stacktrace) Console.WriteLine(e.StackTrace);
                return 1;
            }

            try {
                using (var file = Options.OutputStream) {
                    output_format.Save(file, res);
                }
            } catch (Exception e) {
                Console.WriteLine($"Failed saving file '{Options.OutputPath}': {e.Message}");
                Console.WriteLine($"Are you sure you chose the right format? ({output_format})");
                if (Options.OutputPath != null) {
                    var guessed_format = Formats.GuessFromExtension(Options.OutputPath);

                    if (guessed_format != null && guessed_format.GetType() != output_format.GetType()) {
                        Console.WriteLine($"Did you mean to use the {guessed_format} format?");
                    }
                }
                if (Options.Stacktrace) Console.WriteLine(e.StackTrace);
                return 1;
            }

            return 0;
        }
    }
}
