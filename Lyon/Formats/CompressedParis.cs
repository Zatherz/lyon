﻿using System;
using System.IO;

namespace Lyon.Formats {
    public class CompressedParis : Format {
        public CompressedParis() : base("Compressed Paris", "zpbn", new string[] { "zpbn" }) { }

        public override Resource Load(string path) {
            var helper = new ParisHelper(path: path, compressed: true);
            return new Resource {
                ParisHelper = helper,
                Object = helper.Load()
            };
        }

        public override void Save(Stream stream, Resource res) {
            ParisHelper.Compress(stream, res.ParisHelper.Save(res.Object));
        }
    }
}
