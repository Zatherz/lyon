﻿using System;
using System.IO;

namespace Lyon.Formats {
    public class UncompressedParis : Format {
        public UncompressedParis() : base("Uncompressed Paris", "pbn", new string[] { "pbn" }) { }

        public override Resource Load(string path) {
            var helper = new ParisHelper(path: path, compressed: false);
            return new Resource {
                ParisHelper = helper,
                Object = helper.Load()
            };
        }

        public override void Save(Stream stream, Resource res) {
            res.ParisHelper.Save(stream, res.Object);
        }
    }
}
