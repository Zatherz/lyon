﻿using System;
using System.IO;

namespace Lyon.Formats {
    public class YAML : Format {
        public YAML() : base("YAML", "yaml", new string[] { "yaml", "yml" }) {}

        public override Resource Load(string path) {
            string yaml_text = File.ReadAllText(path);
            return new Resource {
                ParisHelper = new ParisHelper(yaml: yaml_text),
                Object = ParisHelper.Deserialize(yaml_text)
            };
        }

        public override void Save(Stream stream, Resource res) {
            res.ParisHelper.Serialize(stream, res.Object);
        }
    }
}
